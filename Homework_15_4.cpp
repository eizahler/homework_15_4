#include <iostream>

void printNumbers(int N, bool isOdd)
{
	for (int i = 0; i <= N; i++)
	{
		if (i % 2 == 0 && isOdd == false)
		{
			std::cout << i << ' ';
		}
		else if (i % 2 != 0 && isOdd == true)
		{
			std::cout << i << ' ';
		}
	}
	std::cout << '\n' << '\n';
}

int main()
{
	int N = 100;

	for (int i = 0; i <= N; i++)
	{
		if (i % 2 == 0)
		{
			std::cout << i << ' ';
		}
	}
	std::cout << '\n' << '\n';

	printNumbers(100, false);

	printNumbers(100, true);
}